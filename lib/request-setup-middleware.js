var api = require('./api');
var configuration = require('./configuration');
var store = require('./core/storage')

module.exports = function(req, res, next){
     req.api = api(req.token);
     req.config = configuration(req.api).get;
     req.store = store;


     next();
}
