var jwt = require("jsonwebtoken");

module.exports = (function(){
    var key = new Buffer([83, 49, 103, 110, 49, 110, 103, 61, 115, 51, 99, 114, 51, 116]);
   
    return function(req, res, next){
        try {

            if(!req.get('Authorization') || !(req.user = jwt.verify(req.token = req.get('Authorization').replace("Bearer ",""), key, { ignoreExpiration : true }))){
                res.status(401).json({'Message': 'Invalid or malformed JWT'});
                return;
            } 

        } catch (x) {
            res.status(401).json({'Message': 'Invalid or malformed JWT'});
            return;            
        }
        
        next();            
    }

})();

