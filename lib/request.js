var api = require('unirest');
const debug = require('./core/debug')('request');

module.exports = (token, type, endpoint, payload) => {
  //debug(`${type}ing ${endpoint} with payload ${payload}`);
      return new Promise((resolve, reject) => {
          debug(`${type}ing ${endpoint} with payload ${payload}`);

          api[type](endpoint)
          .headers({
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + token
          })
          .send(payload)
          .end((response) => resolve(response.body) );
        });
};
