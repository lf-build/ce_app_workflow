var api = require('unirest');
var Promise = require("bluebird");

function request (type, url, payload, token){
        return new Promise(function (resolve, reject){
            api[type](url)
            .headers({
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            })
            .send(payload)
            .end(function (response) {
                resolve(response);
            });
        });
}

module.exports = function(token) {

 return {
            get: function(url){
                return request('get', url, undefined, token);
            },
            put: function(url, payload){
                    return request('put', url, payload, token);
            },
            post: function(url, payload){
                    return request('post', url, payload, token);
            }
        }
}
