var url = process.env.API_CONFIGURATION_URL || "http://localhost:5008";
var debug = require('debug')('ce_app_workflow:configuration');

module.exports = function (api) {
    return {
        get: function (...key) {
            debug('fetching : ', key);
            return api.get(url + '/' + key[0] + (key[1] ? `/${key[1]}` : ''))
                    .then(function (data) {
                        debug('got', key, data.body);
                        return data.body;
                    }).catch(function(err){
                        debug('failed to fetch', key, err);
                        return err;
                    });
        }
    }
}
