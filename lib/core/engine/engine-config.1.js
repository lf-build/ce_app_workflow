module.exports = {
    "lead": {
        "amount": {
            "set": {
                "$accepts": {
                    "amount": "string(10)"
                },
                "$returns": {
                    "amount": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set(value.amount)",
                "return": "$return('ok', $setStatus({ 'amount': value }))"
            },
        },
        "reason": {
            "set": {
                "$accepts": {
                    "reason": "string(10)"
                },
                "$returns": {
                    "reason": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set(value.reason)",
                "return": "$return('ok', $setStatus({ 'reason': value }))"
            },
        },
        "person": {
            "set": {
                "$accepts": {
                    "title": "mr",
                    "firstName": "Amit",
                    "middleName": "Y",
                    "lastName": "Dave",
                    "email": "amit.d@sigmainfo.net"
                },
                "save-to-store": "$set(value)",
                "return": "$return('ok', $setStatus('person', value))"
            },
            "set-number": {
                "$accepts": {
                    "mobile": "string(10)"
                },
                "$returns": {
                    "mobile": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set('mobile', value.mobile)",
                "set-status": "$setStatus('person', 'mobile', { 'number': value, 'verified': false })",
                "return": "$return('ok', { 'mobile': value.number })"
            },
            "send-otp": {
                "$accepts": "void",
                "$returns": {
                    "ref": "string(50)",
                    "hint": "string(100)"
                },
                "$prerequisites": {
                    "must-have-mobile-number": {
                        "check": "$isSet('mobile')",
                        "pending": [
                            "set-number"
                        ]
                    }
                },
                "get-stupid-number": "$get('mobile')",
                "invoke-otp-service": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'otp'),'/application/123456')",
                    "type": "post",
                    "payload": "{'Phone':  $join('+91', value.number || value) }"
                },
                "save-the-otp-ref": "$set('mobile-otp-ref', value.referenceNumber)",
                "return": "$return('ok', { 'hint': 'verify-otp' })"
            },
            "verify-otp": {
                "$accepts": {
                    "otp": "string(50)"
                },
                "$returns": {
                    "status": "string('verified'|'verification-failed')"
                },
                "$prerequisites": {
                    "must-have-otp-ref-no": {
                        "check": "$isSet('mobile-otp-ref')",
                        "pending": [
                            "send-otp"
                        ]
                    }
                },
                "invoke-verify-otp-service": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints','otp'), '/verify/' , $get('mobile-otp-ref'), '/' + value.otp)",
                    "type": "post"
                },
                "eval-status": "$setStatus('person', 'mobile', 'verified', value.verificationSuccess  ===  true )",
                "return": "$return('ok', $getStatus())"
            },
            "create-account": {
                "$accepts": {
                    "name": "string(50)",
                    "email": "string(50)",
                    "username": "string(50)",
                    "roles": ["string(50)"],
                    "password": "string(50)"
                },
                "$returns": {
                    "status": "string('success'|'failure')"
                },
                "$prerequisites": {
                    "must-have-otp-ref-no": {
                        "check": "$isSet('mobile-otp-ref')",
                        "pending": [
                            "send-otp"
                        ]
                    }
                },
                "set-stuff": "$set('buffer',value)",
                "invoke-account-service": {
                    "activity": "restful-invoke",
                    "endpoint": "$configuration('endpoints','identity')",
                    "type": "put",
                    "payload": "{ 'name': $join($get('firstName'), ' ', $get('lastName')), 'email': $get('email'), 'username': $get('mobile','number'), 'roles':['Marketing'], 'password': value.password }"
                },
                "eval-status": "$set('account', value)",
                "get-buffer": "$get('buffer')",
                "get-access-token-account-service": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints','identity'), '/login')",
                    "type": "post",
                    "payload": "{ 'username': $get('mobile','number'), 'password': value.password }"
                },
                "set-token": "$set('account','token', value.token)",
                "set-account-created": "$setStatus('account', { 'created': true})",
                "return": "$return('ok', $get('account'))"
            }

        },
        "work": {
            "set": {
                "$accepts": {
                    "employerName": "string(10)",
                    "salary": "string(10)",
                    "officialEmail": "string(10)"
                },
                "$returns": {
                    "reason": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set(value)",
                "save-status": "$setStatus(value)",
                "return": "$return('ok',value)"
            },
        },
        "expenses": {
            "set-residence": {
                "$accepts": {
                    "residenceType": "string(10)",
                    "haveHomeLoan": "string(10)",
                    "emi": "string(10)",
                    "rent": "string(10)"

                },
                "$returns": {
                    "reason": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set('residence', value)",
                "save-state": "$setStatus('residence', value)",
                "return": "$return('ok', value )"
            },
            "set-other": {
                "$accepts": {
                    "emi": "string(10)",
                    "other": "string(10)"
                },
                "$returns": {
                    "reason": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set('other', value)",
                "save-state": "$setStatus('expenses', value)",
                "return": "$return('ok',  value  )"
            },
            "set-credit-card": {
                "$accepts": {
                    "hasCreditCard": "string(10)",
                    "balance": "string(10)"
                },
                "$returns": {
                    "reason": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set('credit-card', value)",
                "save-status": "$setStatus('credit-card', value)",
                "return": "$return('ok', value )"
            }
        },
        "details": {
            "get-location-details": {
                "$accepts": {
                    "pin": 384002
                },
                "get-zip-info": {
                    "activity": "restful-invoke",
                    "endpoint": "$join('http://postalpincode.in/api/pincode/', value.pin)",
                    "type": "get"
                },
                "return": "$return('ok', (value && value.PostOffice && value.PostOffice.length) ? { 'District': value.PostOffice[0].District, 'State': value.PostOffice[0].State } : undefined)"
            },
            "set": {
                "$accepts": {
                    "dateOfBirth": "string(10)",
                    "PAN": "string(10)",
                    "adhar": "string(10)",
                    "address": "string(10)",
                    "locality": "string(10)",
                    "city": "string(10)",
                    "pin": "string(10)"
                },
                "$returns": {
                    "reason": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set(value)",
                "save-status": "$setStatus(value)",
                "get-zip-info": {
                    "activity": "restful-invoke",
                    "endpoint": "$join('http://postalpincode.in/api/pincode/', value.pin)",
                    "type": "get"
                },
                "generate-new-value": "$({ PostOffice: value.PostOffice, city: $get('city') })",
                "validate-city": "$setStatus('city-is', { 'valid': value.PostOffice && value.PostOffice.filter(function(postoffice) { return postoffice.District.toLowerCase() === value.city.toLowerCase(); }).length > 0 })",
                "verify-pan": "$setStatus('pan-is', {'valid': true} )",
                "return": "$return('ok', $getStatus())" 
            }
        },
        "application": {
            "test2": ({ value, $facts }) => {

var mobile = "9898344754";
var amount = 220000;
var reason = "Vacation";
var personalDetails = {
    "dateOfBirth":"21/12/1985",
    "PAN":"AFUPJ7365N",
    "address_line_1":"Address Line 1",
    "address_line_2":"Address Line 2",
    "locality":"Locality ",
    "city":"Bangalore",
    "state":"Karnataka",
    "pin":"560056",
    "address_2_line_1":"Address Line 1",
    "address_2_line_2":"Address Line 2",
    "locality_2":"Locality ",
    "city_2":"Bangalore",
    "state_2":"Karnataka",
    "pin_@":"560056"
};
var you = { "title":"Mr.","marital_status":"Married","firstName":"Nitin","middleName":"Y","lastName":"Jani","email":"daveamit@live.com"};

you.gender = you.title === 'Mr.' ? 'Male' : 'Female';
var work = {"employerName":"Sigma Info Solutions","salary":"25000.00","officialEmail":"amit.d@sigmainfo.net"};
var dob = "21/12/1985".split('/');
var residence = {"residenceType":"Self-Owned", "haveHomeLoan": "yes","emi": 4000 ,"rent":""};
var cc = {"hasCreditCard":"yes", "balance": 30000};
var other = {"emi": 50000, "other": 15000};


var obj = {
	"primaryApplicant": {
	//"userId": "57f7ba0a186a8600077d547b",
    "userName": (new Date()).getTime().toString(),
    "password": "!@#123qwe",
	"aadhaarNumber": personalDetails.aadhaarNumber,
	"addresses": 
	  [{
	    "addressLine1": personalDetails.address_line_1 ,
	    "addressLine2": personalDetails.address_line_2,
	    "addressLine3": null,
	    "addressLine4": null,
	    "landMark": personalDetails.locality,
	    "city": personalDetails.city,
	    "state": personalDetails.state,
	    "pinCode": personalDetails.pin,
	    "country": "India",
	    "addressType": "Current",
	    "isDefault": true
	  },
      {
	    "addressLine1": personalDetails.address_2_line_1 ,
	    "addressLine2": personalDetails.address_2_line_2,
	    "addressLine3": null,
	    "addressLine4": null,
	    "landMark": personalDetails.locality_2,
	    "city": personalDetails.city_2,
	    "state": personalDetails.state_2,
	    "pinCode": personalDetails.pin_2,
	    "country": "India",
	    "addressType": "Permanant",
	    "isDefault": true
	  }],
	"dateOfBirth": `${dob[2]}-${dob[1]}-${dob[0]}`,
	"emailAddress": 
	  {
	    "email": you.email,
	    "emailType": "Personal",
	    "isDefault": true
	  },
	 
	"salutation": you.title,
	"firstName": you.firstName,
    "middleName": you.middleName,
	"lastName": you.lastName,
	"permanentAccountNumber": personalDetails.PAN,
	"phoneNumbers": [
	  {
	    "phone": mobile,
	    "countryCode": "91",
	    "phoneType": "Residence",
	    "isDefault": false
	  }]
	,
	"gender": you.gender,
	"maritalStatus": you.marital_status
	},
	"requestedAmount": amount,
	"requestedTermType": "Monthly",
	"purposeOfLoan": reason,
	"source": {
	  "trackingCode": "Code123",
	  "systemChannel": "MerchantPortal",
	  "sourceType": "Merchant",
	  "sourceReferenceId": "Ref123"
	},
	"incomeInformation" : {
	    "income" : work.salary,
	    "paymentFrequency" : "Monthly"
	  },
	"residenceType": "OwnedSelf",
	"selfDeclareExpense": {
	      "monthlyRent": residence.rent || 0,
	      "debtPayments": (residence.emi || 0) + (other.emi || 0),
	      "monthlyExpenses": other.other,
	      "creditCardBalances": cc.balance
	    },
	"employmentDetail": 
	  {
	    "name": work.employerName,
	    "workEmailId": work.officialEmail, 
	    "employmentStatus": "Salaried"
	  }
}




















                
                        return $facts['application'].$get('some-value'); 
                        },
            "company-lookup": {
                "get-company-list": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'company-db'), '/company/search/', value.query)",
                    "type": "get"
                }
            },
            "bank-lookup": {
                "get-bank-list": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'perfios'), '/supported-instition')",
                    "type": "get"
                }
            },
            "start-tran": {
                "get-bank-list": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'perfios'), '/application/66666/start-transaction')",
                    "type": "post",
                    "payload": "value"
                }
            },
            "finish-tran": {
                "get-bank-list": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'perfios'), '/application/66666/', value.perfiosTransactionId,'/complete-transaction')",
                    "type": "post",
                    "payload": "value"
                }
            },
            "fetch-credit-report": {
                "crif-make-inquiry": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'crif-report'), '/workflow/', '12345')",
                    "type": "post",
                    "payload": "{  'name':'Nitin Jain',  'dob':'1981-03-05T00:00:00+05:30',  'Gender' :'MALE',  'VoterId':'AFUPJ7365N',   'MobileNo':'9551542844',  'Addresses': [    {      'type': 'Residence',      'address': '165049,1128,KFC,BANU NAGAR 29TH AVUNUE PUDUR,Silkboard,BANGALORE,KARNATAKA,600053',      'city': 'BANGALORE',      'state': 'Karnataka',      'pin': '600053'    }  ]  }"
                },
                "crif-credit-report": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'crif-report'), '/workflow/', '12345', '/', value.inquiryReferenceNumber, '/', value.reportId)",
                    "type": "get"
                },
                "store": "$setStatus({ 'report': value })",
                "return": "$return('ok', $getStatus())"
            },
            "dry-run-initial-check": {
                "fetch-birth-date": "$($facts['details'].$get('dateOfBirth'))",
                "save-birth-date": "$set('dateOfBirth', value.split('/').reverse().join('-'))",
                "prepare-args": "$({ 'AgeVerify' : { 'DateOfBirth' : $get('dateOfBirth') }, 'CityVerify' : { 'City': $facts['details'].$get('city') }, 'IncomeVerify': { 'Income': $facts['work'].$get('salary') }, 'LoanAmountVerify':{ 'LoanAmount': $facts['amount'].$get() }, 'EmpTypeVerify': { 'EmpType': 'salaried' }, 'EMIToIncome': { 'SelfEMI': 10000, 'Income': $facts['work'].$get('salary') }, 'CCToIncome': {	'SelfBalance': 1000, 'Income': 100000 }, 'AgeGreaterThan58': { 'DateOfBirth' : $get('dateOfBirth') }, 'PAN4thLetter': { 'PAN': $facts['details'].$get('PAN') },'PAN5thLetter': { 'PAN': $facts['details'].$get('PAN'), 'FirstName': $facts['person'].$get('firstName'), 'LastName': $facts['person'].$get('lastName') } } )"
            },
            "run-initial-check": {
                "fetch-birth-date": "$($facts['details'].$get('dateOfBirth'))",
                "save-birth-date": "$set('dateOfBirth', value.split('/').reverse().join('-'))",
                "prepare-args": "$({ 'AgeVerify' : { 'DateOfBirth' : $get('dateOfBirth') }, 'CityVerify' : { 'City': $facts['details'].$get('city') }, 'IncomeVerify': { 'Income': $facts['work'].$get('salary') }, 'LoanAmountVerify':{ 'LoanAmount': $facts['amount'].$get() }, 'EmpTypeVerify': { 'EmpType': 'salaried' }, 'EMIToIncome': { 'SelfEMI': 10000, 'Income': $facts['work'].$get('salary') }, 'CCToIncome': {	'SelfBalance': 1000, 'Income': 100000 }, 'AgeGreaterThan58': { 'DateOfBirth' : $get('dateOfBirth') }, 'PAN4thLetter': { 'PAN': $facts['details'].$get('PAN') },'PAN5thLetter': { 'PAN': $facts['details'].$get('PAN'), 'FirstName': $facts['person'].$get('firstName'), 'LastName': $facts['person'].$get('lastName') } } )",
                "store-prepare-args": "$set('args', value)",
                "eval-eli-rule-by-id": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'rulesengine'),'/PreScreen/EligibilityCheck/1.0.0/evaluate')",
                    "type": "post",
                    "payload": "{ 'Parameter': $get('args') }"
                },
                "save-eli-response": "$set('isEligible', value.preScreen.eligibilityCheck.ageVerify && value.preScreen.eligibilityCheck.cityVerify && value.preScreen.eligibilityCheck.incomeVerify && value.preScreen.eligibilityCheck.loanAmountVerify && value.preScreen.eligibilityCheck.empTypeVerify)",
                "eval-rej-rule-by-id": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'rulesengine'),'/PreScreen/RejectCriteria/1.0.0/evaluate')",
                    "type": "post",
                    "payload": "{ 'Parameter': $get('args') }"
                },
                "save-rej": "$set('isRejected', value.preScreen.rejectCriteria.emiToIncome || value.preScreen.rejectCriteria.ccToIncome || value.preScreen.rejectCriteria.ageGreaterThan58 || value.preScreen.rejectCriteria.paN4thLetter || value.preScreen.rejectCriteria.paN5thLetter)",
                "save-result": "$set('final-result', $({'isRejected': $get('isRejected'), 'isEligible': $get('isEligible')}))",
                "return": "$return('ok', {'isRejected': value.isRejected, 'isEligible': value.isEligible, 'canContinue': value.isRejected === false && value.isEligible === true})"
            },
            "test": (value) => {
                return {
                    'message': value.name
                };
            },
            "submit": {
                "$accepts": {
                    "ipAddress": "10.1.1.99",
                    "workflowId": "workflowId"
                },
                "set1": "$set('cache', { 'IpAddress': value.ipAddress, 'workflowId': value.workflowId})",
                "signCreditPullConsent": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'consent'),'/Application/', value.workflowId, '/CreditPullConsent/sign')",
                    "type": "post",
                    "payload": "{ 'IPAddress': value.IpAddress , 'SignedBy': '1' }"
                },
                "set2": "$set('signCreditPullConsent', value)",
                "get1": "$get('cache')",
                "BankAccInfoConsent": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'consent'),'/Application/', value.workflowId, '/BankAccInfoConsent/sign')",
                    "type": "post",
                    "payload": "{ 'IPAddress': value.IpAddress , 'SignedBy': '1' }"
                },
                "set3": "$set('BankAccInfoConsent', value)",
                "get2": "$get('cache')",
                "EmploymentVerificationConsent": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'consent'),'/Application/', value.workflowId, '/EmploymentVerificationConsent/sign')",
                    "type": "post",
                    "payload": "{ 'IPAddress': value.IpAddress , 'SignedBy': '1' }"
                },
                "set4": "$set('EmploymentVerificationConsent', value)",
                "get3": "$get('cache')",
                "eKYCVerificationConsent": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'consent'),'/Application/', value.workflowId, '/eKYCVerificationConsent/sign')",
                    "type": "post",
                    "payload": "{ 'IPAddress': value.IpAddress , 'SignedBy': '1' }"
                },
                "set5": "$set('eKYCVerificationConsent', value)",
                "get4": "$get('cache')",
                "LocationVerificationConsent": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'consent'),'/Application/', value.workflowId, '/LocationVerificationConsent/sign')",
                    "type": "post",
                    "payload": "{ 'IPAddress': value.IpAddress , 'SignedBy': '1' }"
                },
                "set7": "$set('LocationVerificationConsent', value)",
                "set8": "$set('cache', undefined)",
                "set-status": "$get()",
                "return": "$return('ok', $setStatus(value))"

            },
            "submit-1": {
                "get-personal-info": "$facts['person'].$get()",
                "setstore": "$set('args', { 'id': Math.random() * (500000-200000) + 200000, 'primaryApplicant': {	  'userId': '57f7a0d5186a8600077d5479',	  'salutation': 'Mr',	  'firstName': value.firstName,	  'lastName': value.lastName,	  'middleName': value.middleName,	  'emailAddresses': [	    {	      'email': 'contactsam25@gmail.com',	      'emailType': 'Personal',	      'isDefault': true	    }	  ],	  'aadhaarNumber': '12345',	  'permanentAccountNumber': '45345345',	  'dateOfBirth': '2016-09-02T15:24:28.2480918+05:30',	  'phoneNumbers': [	    {	      'phone': '9898344754',	      'countryCode': '+91',	      'phoneType': 'Residence',	      'isDefault': false	    }	  ],	  'addresses': [	    {	      'addressLine1': 'D',	      'addressLine2': 'LOL',	      'city': 'Bangalore',	      'state': 'Karnataka',	      'pinCode': '380034',	      'country': 'India',	      'addressType': 'Residence',	      'isDefault': false	    }	  ],	  'gender': 'Male',	  'maritalStatus': 'Single',	  'employmentDetails': []  },  'requestedAmount': '300000',  'requestedTermType': 'Monthly',  'requestedTermValue': '12.5',  'purposeOfLoan': 'MovingAndRelocation',  'source': {    'trackingCode': 'Code123',    'systemChannel': 'Merchant',    'sourceType': 'Merchant',    'sourceReferenceId': 'Ref123'  },  'initialOffer': {    'offerId': 'Offer123',    'givenLoanAmount': 278000,    'disbursedLoanAmount': 0,    'annualPercentageRate': 15,    'interestRate': 10,    'repaymentFrequency': 'Monthly',    'repaymentInstallmentAmount': '0',    'numberOfIntallments': '0',    'offerFees': null,    'loanTerm': '1'  },  'employmentDetail': {    'name': 'Sigma',    'workEmailId':     {      'email': 'worktest@gmail.com',      'emailType': 'Personal',      'isDefault': true    },    'employmentStatus': 'Salaried',    'lengthOfEmployment':'5'  },  'residenceType': 'OwnedSelf',  'selfDeclareExpense': {    'monthlyRent': '5000',    'emi': '5000',    'monthlyExpenses': '10000',    'creditCardBalances': '12000'  }})",
                "create-applicant": {
                    "activity": "restful-invoke",
                    "endpoint": "$configuration('endpoints', 'application')",
                    "type": "post",
                    "payload": "$get('args')"
                },
                "set-args-as-value": "$set('args','original-application', value)",
                "set-random-offer-amount": "$set('args', 'initialOffer', 'givenLoanAmount', (Math.random() * (500000-200000) + 200000).toFixed(0))",
                "get-args": "$get('args')"
            }
        }

    }
};
