'use strict';

const debug = require('../debug')('engine:middleware');
const config = require('./engine-config');
//const api = require('../../api')('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTExLTE0VDAwOjUxOjE2LjI0MTczNTZaIiwiZXhwIjoiMjAxNi0xMS0xNFQwMDo1MDo1Ni43Njc0NzY0WiIsInN1YiI6ImpvaG5zbWl0aCIsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpbImFtb3J0aXphdGlvbi9zY2hlZHVsZSIsImxvYW5zL2RvY3VtZW50cy91cGxvYWQiXSwiSXNWYWxpZCI6ZmFsc2V9.F_0aDeBgLpNGBiD5IVVAgEN07swGpnbhg8-yTWFzqX0');
//const config = require('../../configuration')(api);

const storage = require('../storage');
const _ = require('lodash');

const app = require('express')();

// module.exports = (req, res, next) => {
//   debug('middleware running ...');
//   next();
// }

//config.get('app-workflow-engine')
//    .then(init);

init(config);

module.exports = app;

function init(config) {
    const engine = require('./')(config);


    app.post('/:id/:stage/:fact/:action', (req, res, next) => {

        engine.stages[req.params.stage]
            .facts[req.params.fact]
            .actions[req.params.action]
            .$execute(req.params.id, req.body)
            .then(value => {
                res.json(value);
            })
            .catch(err => {
                res.status(400).json(err);
            });
    });

    app.post('/:id/store', (req, res, next) => {
        storage.set
            .bind(storage, 'client-store')(req.params.id, req.body)
            .then(value => {
                res.json(value);
            })
            .catch(err => {
                res.status(400).json(err);
            });
    });


    app.get('/:id/store', (req, res, next) => {
        storage.get
            .bind(storage, 'client-store')(req.params.id)
            .then(value => {
                res.json(value);
            })
            .catch(err => {
                res.status(400).json(err);
            });
    });

    app.get('/:id/:stage/:fact', (req, res, next) => {
        storage.get
            .bind(storage, 'status', req.params.stage, req.params.fact)()
            .then(value => {
                res.json(value[req.params.id]);
            })
            .catch(err => {
                res.status(400).json(err);
            });
    });

    app.get('/:id/:stage', (req, res, next) => {
        storage.get
            .bind(storage, 'status', req.params.stage)()
            .then(value => {
                res.json(_.reduce(value, (previous, current, key) => {
                    console.log(current);
                    previous[key] = current[req.params.id];
                    return previous;
                }, {}));
            })
            .catch(err => {
                res.status(400).json(err);
            });
    });
}
