const debug = require('../debug')('engine:action');
const createActivity = require('./activity');

const _ = require('lodash');

module.exports = function (fact, name, configuration) {
  var self = { };

  //Register this activity with fact.
  fact.actions[name] = self;

  self.fact = fact;
  self.name = name;
  self.$set =  fact.$set; //.bind(fact, name);
  self.$get = fact.$get; //.bind(fact, name);
  self.$transform = fact.$transform.bind(fact, name);
  self.$setStatus = fact.$setStatus; //.bind(fact, name);
  self.$getStatus = fact.$getStatus; //.bind(fact, name);
  self.activities = { };
  self.createActivity = createActivity.bind(createActivity, self);

 if(typeof configuration === 'function'){
   debug(name, 'is a direct lambda');
   debug('lambda set as default activity');
  //  self.activities.default = {
  //    name: 'default',
  //    $execute: (requestId, value) => new Promise((resolve, reject) => {
  //                                       var result = configuration(value);
  //                                       resolve(result);
  //                                     }) 
  //  };

  self.createActivity('default',  {
    activity: 'lambda',
    body: configuration
  });
 }
 else{
    _.forEach(configuration, (activity, name) => {
        debug('loading activity', name);
        self.createActivity(name, activity);
    });
  }  

  self.$execute = (requestId, value) => {

    debug('executing', requestId, value)
    return  _.reduce(self.activities, (previous, current) => {
      debug(current.$execute);
                  if(current.name.startsWith('$')){
                    return previous;
                  }


                return previous
                ? previous
                      .then(value => { 
                        return current.$execute(requestId, value); 
                      })
                      .catch(err => { 
                        debug(`Failed to entertain ${requestId} while performing ${current.name}`, value, err);
                        throw err;
                      })
                : ( () => { 
                    debug('xxxxxxx', current.name); 
                    return current.$execute(requestId, value); 
                  } )()
                  .catch(err => { 
                    debug(`Failed to entertain ${requestId} while performing ${current.name} (2)`, value, err);
                    throw err;
                  });
              }, undefined);
  };


  return self;
};
