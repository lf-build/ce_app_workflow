const debug = require('../debug')('engine:stage');
const storage = require('../storage');
const transformer = require('../transformer');
const fact = require('./fact');
const _ = require('lodash');
module.exports = function (name, configuration) {
  var self = { };

  self.name = name;
  self.$set = storage.set.bind(storage, 'store', name);
  self.$get = storage.get.bind(storage, 'store', name);
  self.$transform = transformer;
  self.$setStatus = storage.set.bind(storage, 'status', name);
  self.$getStatus = storage.get.bind(storage, 'status', name);

  self.createFact = fact.bind(fact, self);
  self.facts = {};


  _.forEach(configuration, (fact, name) => {
    debug('loading fact', name);
    self.createFact(name, fact);
  });

  return self;
};
