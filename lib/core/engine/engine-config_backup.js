module.exports = {
    "lead": {
        "amount": {
            "set": {
                "$accepts": {
                    "amount": "string(10)"
                },
                "$returns": {
                    "amount": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set(value.amount)",
                "return": "$return('ok', $setStatus({ 'amount': value }))"
            }
        },
        "reason": {
            "set": {
                "$accepts": {
                    "reason": "string(10)"
                },
                "$returns": {
                    "reason": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set(value.reason)",
                "return": "$return('ok', $setStatus({ 'reason': value }))"
            }
        },
        "person": {
            "set": {
                "$accepts": {
                    "title": "mr",
                    "firstName": "Amit",
                    "middleName": "Y",
                    "lastName": "Dave",
                    "email": "amit.d@sigmainfo.net"
                },
                "save-to-store": "$set(value)",
                "return": "$return('ok', $setStatus('person', value))"
            },
            "set-number": {
                "$accepts": {
                    "mobile": "string(10)"
                },
                "$returns": {
                    "mobile": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set('mobile', value.mobile)",
                "set-status": "$setStatus('person', 'mobile', { 'number': value, 'verified': false })",
                "return": "$return('ok', { 'mobile': value.number })"
            },
            "send-otp": {
                "$accepts": "void",
                "$returns": {
                    "ref": "string(50)",
                    "hint": "string(100)"
                },
                "$prerequisites": {
                    "must-have-mobile-number": {
                        "check": "$isSet('mobile')",
                        "pending": ["set-number"]
                    }
                },
                "get-stupid-number": "$get('mobile')",
                "invoke-otp-service": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'otp'),'/application/123456')",
                    "type": "post",
                    "payload": "{'Phone':  $join('+91', value.number || value) }"
                },
                "save-the-otp-ref": "$set('mobile-otp-ref', value.referenceNumber)",
                "return": "$return('ok', { 'hint': 'verify-otp' })"
            },
            "verify-otp": {
                "$accepts": {
                    "otp": "string(50)"
                },
                "$returns": {
                    "status": "string('verified'|'verification-failed')"
                },
                "$prerequisites": {
                    "must-have-otp-ref-no": {
                        "check": "$isSet('mobile-otp-ref')",
                        "pending": ["send-otp"]
                    }
                },
                "invoke-verify-otp-service": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints','otp'), '/verify/' , $get('mobile-otp-ref'), '/' + value.otp)",
                    "type": "post"
                },
                "eval-status": "$setStatus('person', 'mobile', 'verified', value.verificationSuccess  ===  true )",
                "return": "$return('ok', $getStatus())"
            },
            "create-account": {
                "$accepts": {
                    "name": "string(50)",
                    "email": "string(50)",
                    "username": "string(50)",
                    "roles": ["string(50)"],
                    "password": "string(50)"
                },
                "$returns": {
                    "status": "string('success'|'failure')"
                },
                "$prerequisites": {
                    "must-have-otp-ref-no": {
                        "check": "$isSet('mobile-otp-ref')",
                        "pending": ["send-otp"]
                    }
                },
                "set-stuff": "$set('buffer',value)",
                "invoke-account-service": {
                    "activity": "restful-invoke",
                    "endpoint": "$configuration('endpoints','identity')",
                    "type": "put",
                    "payload": "{ 'name': $join($get('firstName'), ' ', $get('lastName')), 'email': $get('email'), 'username': $get('mobile','number'), 'roles':['Marketing'], 'password': value.password }"
                },
                "eval-status": "$set('account', value)",
                "get-buffer": "$get('buffer')",
                "get-access-token-account-service": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints','identity'), '/login')",
                    "type": "post",
                    "payload": "{ 'username': $get('mobile','number'), 'password': value.password }"
                },
                "set-token": "$set('account','token', value.token)",
                "set-account-created": "$setStatus('account', { 'created': true})",
                "return": "$return('ok', $get('account'))"
            }

        },
        "work": {
            "set": {
                "$accepts": {
                    "employerName": "string(10)",
                    "salary": "string(10)",
                    "officialEmail": "string(10)"
                },
                "$returns": {
                    "reason": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set(value)",
                "save-status": "$setStatus(value)",
                "return": "$return('ok',value)"
            }
        },
        "expenses": {
            "set-residence": {
                "$accepts": {
                    "residenceType": "string(10)",
                    "haveHomeLoan": "string(10)",
                    "emi": "string(10)",
                    "rent": "string(10)"

                },
                "$returns": {
                    "reason": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set('residence', value)",
                "save-state": "$setStatus('residence', value)",
                "return": "$return('ok', value )"
            },
            "set-other": {
                "$accepts": {
                    "emi": "string(10)",
                    "other": "string(10)"
                },
                "$returns": {
                    "reason": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set('other', value)",
                "save-state": "$setStatus('expenses', value)",
                "return": "$return('ok',  value  )"
            },
            "set-credit-card": {
                "$accepts": {
                    "hasCreditCard": "string(10)",
                    "balance": "string(10)"
                },
                "$returns": {
                    "reason": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set('credit-card', value)",
                "save-status": "$setStatus('credit-card', value)",
                "return": "$return('ok', value )"
            }
        },
        "details": {
            "get-location-details": {
                "$accepts": {
                    "pin": 384002
                },
                "get-zip-info": {
                    "activity": "restful-invoke",
                    "endpoint": "$join('http://postalpincode.in/api/pincode/', value.pin)",
                    "type": "get"
                },
                "return": "$return('ok', (value && value.PostOffice && value.PostOffice.length) ? { 'District': value.PostOffice[0].District, 'State': value.PostOffice[0].State } : undefined)"
            },
            "set": {
                "$accepts": {
                    "dateOfBirth": "string(10)",
                    "PAN": "string(10)",
                    "adhar": "string(10)",
                    "address": "string(10)",
                    "locality": "string(10)",
                    "city": "string(10)",
                    "pin": "string(10)"
                },
                "$returns": {
                    "reason": "string(10)",
                    "hint": "string(100)"
                },
                "save-to-store": "$set(value)",
                "save-status": "$setStatus(value)",
                "get-zip-info": {
                    "activity": "restful-invoke",
                    "endpoint": "$join('http://postalpincode.in/api/pincode/', value.pin)",
                    "type": "get"
                },
                "generate-new-value": "$({ PostOffice: value.PostOffice, city: $get('city') })",
                "validate-city": "$setStatus('city-is', { 'valid': value.PostOffice && value.PostOffice.filter(function(postoffice) { return postoffice.District.toLowerCase() === value.city.toLowerCase(); }).length > 0 })",
                "verify-pan": "$setStatus('pan-is', {'valid': true} )",
                "return": "$return('ok', $getStatus())"
            }
        },
        "application": {
            "create": ({
                value,
                $facts,
                $configuration,
                $api,
                $set,
                $setStatus
            }) => {

                return Promise.all([
                    $facts['amount'].$get(),
                    $facts['reason'].$get(),
                    $facts['person'].$get(),
                    $facts['work'].$get(),
                    $facts['expenses'].$get(),
                    $facts['details'].$get()
                ]).then((datas) => {
                    const [amount,
                        _reason,
                        you,
                        work,
                        expenses,
                        personalDetails
                    ] = datas;
                    const {
                        residence,
                        'credit-card': cc,
                        other
                    } = expenses;

                    var reason = "HomeImprovement";
                    const mobile = you.mobile.number;
                    you.gender = you.title === 'Mr.' ?
                        'Male' :
                        'Female';
                    var dob = personalDetails.dateOfBirth.split('/');

                    var obj = {
                        "primaryApplicant": {
                            //"userId": "57f7ba0a186a8600077d547b",
                            "userName": (new Date()).getTime().toString(),
                            "password": "!@#123qwe",
                            "aadhaarNumber": personalDetails.aadhaarNumber,
                            "addresses": [{
                                "addressLine1": personalDetails.address_line_1,
                                "addressLine2": personalDetails.address_line_2,
                                "addressLine3": null,
                                "addressLine4": null,
                                "landMark": personalDetails.locality,
                                "city": personalDetails.city,
                                "state": personalDetails.state,
                                "pinCode": personalDetails.pin,
                                "country": "India",
                                "addressType": "Current",
                                "isDefault": true
                            }, {
                                "addressLine1": personalDetails.address_2_line_1,
                                "addressLine2": personalDetails.address_2_line_2,
                                "addressLine3": null,
                                "addressLine4": null,
                                "landMark": personalDetails.locality_2,
                                "city": personalDetails.city_2,
                                "state": personalDetails.state_2,
                                "pinCode": personalDetails.pin_2,
                                "country": "India",
                                "addressType": "Permanant",
                                "isDefault": true
                            }],
                            "dateOfBirth": `${dob[2]}-${dob[1]}-${dob[0]}`,
                            "emailAddress": {
                                "email": you.email,
                                "emailType": "Personal",
                                "isDefault": true
                            },

                            "salutation": you.title,
                            "firstName": you.firstName,
                            "middleName": you.middleName,
                            "lastName": you.lastName,
                            "permanentAccountNumber": personalDetails.PAN,
                            "phoneNumbers": [{
                                "phone": mobile,
                                "countryCode": "91",
                                "phoneType": "Residence",
                                "isDefault": false
                            }],
                            "gender": you.gender,
                            "maritalStatus": you.marital_status,
                            "employmentDetail": {
                                "incomeInformation": {
                                    "income": work.salary

                                },
                                "name": work.employerName,
                                "workEmail": work.officialEmail,
                                "employmentStatus": "Salaried"
                            }
                        },
                        "requestedAmount": amount,
                        "requestedTermType": "Monthly",
                        "purposeOfLoan": reason,
                        "source": {
                            "trackingCode": "Code123",
                            "systemChannel": "MerchantPortal",
                            "sourceType": "Merchant",
                            "sourceReferenceId": "Ref123"
                        },
                        "incomeInformation": {
                            "income": work.salary,
                            "paymentFrequency": "Monthly"
                        },
                        "residenceType": "Owned-self",
                        "selfDeclareExpense": {
                            "monthlyRent": residence.rent || 0,
                            "debtPayments": (residence.emi || 0) + (other.emi || 0),
                            "monthlyExpenses": other.other,
                            "creditCardBalances": cc.balance
                        },
                        "employmentDetail": {
                            "incomeInformation": {
                                "income": work.salary

                            },
                            "name": work.employerName,
                            "workEmail": work.officialEmail,
                            "employmentStatus": "Salaried"
                        }
                    };

                    return $configuration('endpoints', 'application').then((endpoint) => {
                        return $api.post(endpoint, obj).then((app) => {
                            $set('app', app.body);
                            $set('app', 'raw', obj);
                            $set('applicationNumber', app.body.applicationNumber);
                            $setStatus('applicationNumber', app.body.applicationNumber);
                            return {
                                applicationNumber: app.body.applicationNumber
                            };
                        });;
                    });
                });

            },
            "invok-social-verification": {
                "load-company": "$get('app')",
                "invoke-lendo": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'lendo'), '/application/', value.applicationNumber, '/', value.applicationNumber, '/score')",
                    "type": "get"
                }
            },
            "company-lookup": {
                "get-company-list": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'company-db'), '/application/xyz/company/search/', value.query)",
                    "type": "get"
                },
                "return-sane-data": '$return("ok", value.companySearchResponse)'
            },
            "invoke-company-cat": {
                "load-company": "$({ 'company': $get('company'),'app': $get('app') })",
                "invoke": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'company-db'), '/application/', value.app.applicationNumber ,'/company/category/', value.company.cin)",
                    "type": "get"
                },
            },
            "set-company":{
                "set": "$set('company',value)"
            },
            "bank-lookup": {
                "get-bank-list": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'perfios'), '/supported-instition')",
                    "type": "get"
                }
            },
            "set-fake-app-number": {
                "set-app-num": "$set('applicationNumber', value.applicationNumber)",
                "set-app-num-in-app": "$set('app', { applicationNumber: value, 'raw': { primaryApplicant: {firstName: 'Amit', lastName: 'Dave'} } })",
                "set-app-num-in-app-status": "$setStatus('app','applicationNumber', value.applicationNumber)"
            },
            "start-tran": {
                "get-cache": "$set('trans-args', value)",
                "get-app-id": "$facts['application'].$get('applicationNumber')",
                "get-bank-list": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'perfios'), '/application/', value ,'/start-transaction')",
                    "type": "post",
                    "payload": "$get('trans-args')"
                }
            },
            "finish-tran": {
                "get-app-id": "$get('applicationNumber')",
                "finish-perfios-trans": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'perfios'), '/application/', value ,'/complete-transaction')",
                    "type": "post",
                    "payload": "{}"
                },
                "pull-perfios-xml": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'perfios'), '/application/', $get('applicationNumber') ,'/xml')",
                    "type": "get"
                }
            },
            "fetch-credit-report": {
                "crif-make-inquiry": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'crif-report'), '/workflow/', '12345')",
                    "type": "post",
                    "payload": "{  'name':'Nitin Jain',  'dob':'1981-03-05T00:00:00+05:30',  'Gender' :'MALE',  'VoterId':'AFUPJ7365N',   'MobileNo':'9551542844',  'Addresses': [    {      'type': 'Residence',      'address': '165049,1128,KFC,BANU NAGAR 29TH AVUNUE PUDUR,Silkboard,BANGALORE,KARNATAKA,600053',      'city': 'BANGALORE',      'state': 'Karnataka',      'pin': '600053'    }  ]  }"
                },
                "crif-credit-report": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'crif-report'), '/workflow/', '12345', '/', value.inquiryReferenceNumber, '/', value.reportId)",
                    "type": "get"
                },
                "store": "$setStatus({ 'report': value })",
                "return": "$return('ok', $getStatus())"
            },
            "dry-run-initial-check": {
                "fetch-birth-date": "$($facts['details'].$get('dateOfBirth'))",
                "save-birth-date": "$set('dateOfBirth', value.split('/').reverse().join('-'))",
                "prepare-args": "$({ 'AgeVerify' : { 'DateOfBirth' : $get('dateOfBirth') }, 'CityVerify' : { 'City': $facts['details'].$get('city') }, 'IncomeVerify': { 'Income': $facts['work'].$get('salary') }, 'LoanAmountVerify':{ 'LoanAmount': $facts['amount'].$get() }, 'EmpTypeVerify': { 'EmpType': 'salaried' }, 'EMIToIncome': { 'SelfEMI': 10000, 'Income': $facts['work'].$get('salary') }, 'CCToIncome': {	'SelfBalance': 1000, 'Income': 100000 }, 'AgeGreaterThan58': { 'DateOfBirth' : $get('dateOfBirth') }, 'PAN4thLetter': { 'PAN': $facts['details'].$get('PAN') },'PAN5thLetter': { 'PAN': $facts['details'].$get('PAN'), 'FirstName': $facts['person'].$get('firstName'), 'LastName': $facts['person'].$get('lastName') } } )"
            },
            "run-initial-check": {
                "fetch-birth-date": "$($facts['details'].$get('dateOfBirth'))",
                "save-birth-date": "$set('dateOfBirth', value.split('/').reverse().join('-'))",
                "prepare-args": "$({ 'AgeVerify' : { 'DateOfBirth' : $get('dateOfBirth') }, 'CityVerify' : { 'City': $facts['details'].$get('city') }, 'IncomeVerify': { 'Income': $facts['work'].$get('salary') }, 'LoanAmountVerify':{ 'LoanAmount': $facts['amount'].$get() }, 'EmpTypeVerify': { 'EmpType': 'salaried' }, 'EMIToIncome': { 'SelfEMI': 10000, 'Income': $facts['work'].$get('salary') }, 'CCToIncome': {	'SelfBalance': 1000, 'Income': 100000 }, 'AgeGreaterThan58': { 'DateOfBirth' : $get('dateOfBirth') }, 'PAN4thLetter': { 'PAN': $facts['details'].$get('PAN') },'PAN5thLetter': { 'PAN': $facts['details'].$get('PAN'), 'FirstName': $facts['person'].$get('firstName'), 'LastName': $facts['person'].$get('lastName') } } )",
                "store-prepare-args": "$set('args', value)",
                "eval-eli-rule-by-id": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'rulesengine'),'/PreScreen/EligibilityCheck/1.0.0/evaluate')",
                    "type": "post",
                    "payload": "{ 'Parameter': $get('args') }"
                },
                "save-eli-response": "$set('isEligible', value.preScreen.eligibilityCheck.ageVerify && value.preScreen.eligibilityCheck.cityVerify && value.preScreen.eligibilityCheck.incomeVerify && value.preScreen.eligibilityCheck.loanAmountVerify && value.preScreen.eligibilityCheck.empTypeVerify)",
                "eval-rej-rule-by-id": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'rulesengine'),'/PreScreen/RejectCriteria/1.0.0/evaluate')",
                    "type": "post",
                    "payload": "{ 'Parameter': $get('args') }"
                },
                "save-rej": "$set('isRejected', value.preScreen.rejectCriteria.emiToIncome || value.preScreen.rejectCriteria.ccToIncome || value.preScreen.rejectCriteria.ageGreaterThan58 || value.preScreen.rejectCriteria.paN4thLetter || value.preScreen.rejectCriteria.paN5thLetter)",
                "save-result": "$set('final-result', $({'isRejected': $get('isRejected'), 'isEligible': $get('isEligible')}))",
                "return": "$return('ok', {'isRejected': value.isRejected, 'isEligible': value.isEligible, 'canContinue': value.isRejected === false && value.isEligible === true})"
            },
            "test": (value) => {
                return {
                    'message': value.name
                };
            },
            "sign-pull-consent": {
                "$accepts": {
                    "ipAddress": "10.1.1.99",
                    "workflowId": "workflowId"
                },
                "set1": "$set('cache', { 'IpAddress': value.ipAddress, 'workflowId': value.workflowId})",
                "signCreditPullConsent": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'consent'),'/Application/', value.workflowId, '/CreditPullConsent/sign')",
                    "type": "post",
                    "payload": "{ 'IPAddress': value.IpAddress , 'SignedBy': '1' }"
                },
                "set2": "$set('signCreditPullConsent', value)",
                "get1": "$get('cache')",
                "BankAccInfoConsent": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'consent'),'/Application/', value.workflowId, '/BankAccInfoConsent/sign')",
                    "type": "post",
                    "payload": "{ 'IPAddress': value.IpAddress , 'SignedBy': '1' }"
                },
                "set3": "$set('BankAccInfoConsent', value)",
                "get2": "$get('cache')",
                "EmploymentVerificationConsent": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'consent'),'/Application/', value.workflowId, '/EmploymentVerificationConsent/sign')",
                    "type": "post",
                    "payload": "{ 'IPAddress': value.IpAddress , 'SignedBy': '1' }"
                },
                "set4": "$set('EmploymentVerificationConsent', value)",
                "get3": "$get('cache')",
                "eKYCVerificationConsent": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'consent'),'/Application/', value.workflowId, '/eKYCVerificationConsent/sign')",
                    "type": "post",
                    "payload": "{ 'IPAddress': value.IpAddress , 'SignedBy': '1' }"
                },
                "set5": "$set('eKYCVerificationConsent', value)",
                "get4": "$get('cache')",
                "LocationVerificationConsent": {
                    "activity": "restful-invoke",
                    "endpoint": "$join($configuration('endpoints', 'consent'),'/Application/', value.workflowId, '/LocationVerificationConsent/sign')",
                    "type": "post",
                    "payload": "{ 'IPAddress': value.IpAddress , 'SignedBy': '1' }"
                },
                "set7": "$set('LocationVerificationConsent', value)",
                "set8": "$set('cache', undefined)",
                "set-status": "$get()",
                "return": "$return('ok', $setStatus(value))"

            }
        },
        "offer": {
            "generate": ({
                $facts,
                $join,
                $configuration,
                $api,
                $set,
                $setStatus,
                $debug
            }) => {

                return Promise.all([
                    $configuration('endpoints', 'wsproxy'),
                    $facts['application'].$get('app'),
                    $configuration('endpoints', 'scorecard')
                ]).then(([wsProxy, app, scoreCardEndpoint]) => {
                    const applicant = {
                        firstName: app.raw.primaryApplicant.firstName,
                        lastName: app.raw.primaryApplicant.lastName
                    }
                    return new Promise((resolve, reject) => {
                        var WebSocket = require('ws');
                        var ws = new WebSocket(wsProxy);

                        ws.on('open', function open() {
                            $debug('Connected to event hub, waiting for authorization ...');
                            ws.send(JSON.stringify({
                                'type': 'authenticate',
                                'events': [`InitialOfferProcessed_${app.applicationNumber}`],
                                'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTExLTE0VDAwOjUxOjE2LjI0MTczNTZaIiwiZXhwIjoiMjAxNi0xMS0xNFQwMDo1MDo1Ni43Njc0NzY0WiIsInN1YiI6ImpvaG5zbWl0aCIsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpbImFtb3J0aXphdGlvbi9zY2hlZHVsZSIsImxvYW5zL2RvY3VtZW50cy91cGxvYWQiXSwiSXNWYWxpZCI6ZmFsc2V9.F_0aDeBgLpNGBiD5IVVAgEN07swGpnbhg8-yTWFzqX0'
                            }));

                        });

                        ws.on('message', function (message, flags) {
                            event = JSON.parse(message);

                            if (event.type === 'authorized') {
                                $debug('Authorized');
                                $debug(`Waiting for "InitialOfferProcessed_${app.applicationNumber}"`);

                                $debug('getting offer', `${scoreCardEndpoint}/application/${app.applicationNumber}/initialoffer`);
                                $api.post(`${scoreCardEndpoint}/application/${app.applicationNumber}/initialoffer`, {}).then((response) => {
                                    if (response.code !== 200) {
                                        reject({
                                            applicant,
                                            "Status": "Failed",
                                            "Amount": -1,
                                            "ApplicationNumber": app.applicationNumber,
                                            "Reasons": {
                                                code: response.code
                                            }
                                        });
                                    }
                                }).catch((e) => {
                                    reject({
                                        applicant,
                                        "Status": "Failed",
                                        "Amount": -1,
                                        "ApplicationNumber": app.applicationNumber,
                                        "Reasons": e
                                    });

                                    $debug('returning because of an error', e);
                                });

                                return;
                            }

                            if (event.name.startsWith('InitialOfferProcessed')) {
                                $debug('Got offer ', JSON.stringify(event.data.InitialOffer));
                                //Detect failure and do an early exit.
                                if (!(event.data.InitialOffer.Offers && event.data.InitialOffer.Offers.length)) {

                                    reject({
                                        applicant,
                                        "Status": "Failed",
                                        "Amount": -1,
                                        "ApplicationNumber": app.applicationNumber,
                                        "Reasons": event.data.InitialOffer.Reasons
                                    });
                                    $debug('Returning as Offers was null or empty');
                                    return;
                                }

                                var selectedOffer = event.data.InitialOffer.Offers.reduce((a, c) => a.OfferAmount > c.OfferAmount ?
                                    a :
                                    c, {
                                        OfferAmount: 0
                                    });
                                if (selectedOffer) {

                                    const offer = {
                                        applicant,
                                        "Status": "Success",
                                        "Amount": selectedOffer.OfferAmount,
                                        "ApplicationNumber": app.applicationNumber
                                    };

                                    $set('initialOffer', offer);
                                    $setStatus('initialOffer', offer);
                                    resolve(offer);
                                    $debug('Offer generated', offer);
                                } else {
                                    reject({
                                        applicant,
                                        "Status": "Failed",
                                        "Amount": -1,
                                        "ApplicationNumber": app.applicationNumber,
                                        "Reasons": event.data.InitialOffer.Reasons
                                    });
                                    $debug('Rejected beause api rejected the offer', event.data.InitialOffer);
                                }
                            }
                        });

                    });
                });
            },

            "generate-final": ({
                $facts,
                $join,
                $configuration,
                $api,
                $set,
                $setStatus,
                $debug
            }) => {

                return Promise.all([
                    $configuration('endpoints', 'wsproxy'),
                    $facts['application'].$get('app'),
                    $configuration('endpoints', 'scorecard')
                ]).then(([wsProxy, app, scoreCardEndpoint]) => {
                    const applicant = {
                        firstName: app.raw.primaryApplicant.firstName,
                        lastName: app.raw.primaryApplicant.lastName
                    }
                    return new Promise((resolve, reject) => {
                        var WebSocket = require('ws');
                        var ws = new WebSocket(wsProxy);

                        ws.on('open', function open() {
                            $debug('Connected to event hub, waiting for authorization ...');
                            ws.send(JSON.stringify({
                                'type': 'authenticate',
                                'events': [`FinalOfferProcessed_${app.applicationNumber}`],
                                'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTExLTE0VDAwOjUxOjE2LjI0MTczNTZaIiwiZXhwIjoiMjAxNi0xMS0xNFQwMDo1MDo1Ni43Njc0NzY0WiIsInN1YiI6ImpvaG5zbWl0aCIsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpbImFtb3J0aXphdGlvbi9zY2hlZHVsZSIsImxvYW5zL2RvY3VtZW50cy91cGxvYWQiXSwiSXNWYWxpZCI6ZmFsc2V9.F_0aDeBgLpNGBiD5IVVAgEN07swGpnbhg8-yTWFzqX0'
                            }));

                        });

                        ws.on('message', function (message, flags) {
                            event = JSON.parse(message);

                            if (event.type === 'authorized') {
                                $debug('Authorized');
                                $debug(`Waiting for "FinalOfferProcessed_${app.applicationNumber}"`);

                                $debug('getting offer', `${scoreCardEndpoint}/application/${app.applicationNumber}/finaloffer`);
                                $api.post(`${scoreCardEndpoint}/application/${app.applicationNumber}/finaloffer`, {}).then((response) => {
                                    if (response.code !== 200) {
                                        reject({
                                            applicant,
                                            "Status": "Failed",
                                            "Amount": -1,
                                            "ApplicationNumber": app.applicationNumber,
                                            "Reasons": {
                                                code: response.code
                                            }
                                        });
                                    }
                                }).catch((e) => {
                                    reject({
                                        applicant,
                                        "Status": "Failed",
                                        "Amount": -1,
                                        "ApplicationNumber": app.applicationNumber,
                                        "Reasons": e
                                    });

                                    $debug('returning because of an error', e);
                                });

                                return;
                            }

                            if (event.name.startsWith('FinalOfferProcessed')) {
                                $debug('Got offer ', JSON.stringify(event.data.FinalOffer));
                                //Detect failure and do an early exit.
                                if (!(event.data.FinalOffer.FinalOffers && event.data.FinalOffer.FinalOffers.length)) {

                                    reject({
                                        applicant,
                                        "Status": "Failed",
                                        "offers": undefined,
                                        "ApplicationNumber": app.applicationNumber,
                                        "Reasons": event.data.FinalOffer.Reasons
                                    });
                                    $debug('Returning as FinalOffers was null or empty');
                                    return;
                                }

                                var selectedOffers = event.data.FinalOffer.FinalOffers.filter((a, c) => a.OfferAmount > 0);
                                if (selectedOffers && selectedOffers.length) {

                                    const offer = {
                                        applicant,
                                        "Status": "Success",
                                        "Offers": selectedOffers,
                                        "ApplicationNumber": app.applicationNumber
                                    };

                                    $set('finalOffer', offer);
                                    $setStatus('finalOffer', offer);
                                    resolve(offer);
                                    $debug('Final Offer generated', offer);
                                } else {
                                    reject({
                                        applicant,
                                        "Status": "Failed",
                                        "Offers": undefined,
                                        "ApplicationNumber": app.applicationNumber,
                                        "Reasons": event.data.FinalOffer.Reasons
                                    });
                                    $debug('Rejected beause api rejected the Final Offer', event.data.FinalOffer);
                                }
                            }
                        });

                    });
                });
            }
        }

    }
}