const debug = require('../debug')('engine');
const createStage = require('./stage');
const _ = require('lodash');

module.exports = (config) => {
  var self = { };

  debug('******************************* Engine is loading now ************************')
  self.stages = _.reduce(config, (previous, current, prop) => {
      previous[prop] = createStage(prop, current);
      return previous;
  }, { });
  debug('******************** Engine is ready to rock and roll ************************')

  return self;
};
