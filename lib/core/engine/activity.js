'use strict';
const Debug = require('../debug');
const debug = Debug('engine:activity');
const _ = require('lodash');
const interpolate = require('../interpolate');
const apiStub = require('../../api');
const configurationStub = require('../../configuration');

module.exports = function (action, name, configuration) {
  var self = { };

  //Register this activity with action.
  action.activities[name] = self;

  self.name = name;
  self.$set =  action.$set; //.bind(action, name);
  self.$get = action.$get; //.bind(action, name);
  self.$transform = action.$transform; //.bind(action, name);
  self.$setStatus = action.$setStatus; //.bind(action, name);
  self.$getStatus = action.$getStatus; //.bind(action, name);
  self.$hint = () => action.name + '/' + name;
  self.$return = (status, returnValue) => interpolate(returnValue);
  self.$configuration = configurationStub;
  self.$join = (...args) => {

    return Promise.all(_.map(args, (item, index) => {
      if(item && _.isFunction(item.then)){
        return item.then((value) => {
          args[index] = value;
          return value;
        });
      }else {
        return new Promise((resolve, reject) => resolve(item));
      }
    })).then((value) => {
      debug('returning join string', value.join(''));
      return value.join('');
    });
  };

  
  if(name.startsWith('$')){
    switch (name) {
      case '$accepts':
        action[name] = configuration;
        break;
      case '$returns':
        action[name] = configuration;
        break;
      case '$prerequisites':
        action[name] = configuration;
        break;
       default:
        debug('x-x-x-x-x-x-x-x-x-x-x--->>> (unknown) Node to process', configuration);
    }

    if(typeof configuration === 'string'){
        debug('unknown stuff!!', name, configuration);
    }else {
        debug('x-x-x-x-x-x-x-x-x-x-x--->>> Node to process', configuration, name);

        switch (name) {
          case '$prerequisites':
                action['$prerequisites'] = {  } 
                
                // _.reduce(configuration, (p, c, prop) => {
                //   var isSet = (key) => self.get(key)
                //                            .then((value) => {
                //                              return value !== undefined;
                //                            });


                //   var check = new Function('$get', '$isSet', '$getStatus', 'new Promise(function(r){r(\'false\')})');
                //   p[prop] = new Promise((resolve, reject) => check()
                //                                         .then((value) => {
                //                                           if(value){
                //                                             resolve(value);
                //                                           } else {
                //                                             reject(c.pending)
                //                                           }
                //                                         })
                //                         ) ;
                // }, {})
            break;
          default:

        }
    }

  }else {
    if(typeof configuration === 'string'){
      self.$body =
      new Function('$set', '$get', '$transform',
                                   '$setStatus','$getStatus',
                                   '$hint', '$return','$facts', '$', 'value',
                                   `return ${configuration}`);

      self.$execute = (requestId, value) => {

        return self.$body(
          self.$set.bind(self, requestId),
          self.$get.bind(self, requestId),
          self.$transform,
          self.$setStatus.bind(self, requestId),
          self.$getStatus.bind(self, requestId),
          self.$hint,
          self.$return,
          _.reduce(action.fact.stage.facts, (p, c, prop) => {
            p[prop] = {
              '$get': c.$get.bind(self, requestId)
            };
            return p;
          }, {}) ,
          interpolate,
          value
        );
      };

    }else {
        debug('x-x-x-x-x-x-x-x-x-x-x--->>> Node to process', configuration);
        var token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTExLTE0VDAwOjUxOjE2LjI0MTczNTZaIiwiZXhwIjoiMjAxNi0xMS0xNFQwMDo1MDo1Ni43Njc0NzY0WiIsInN1YiI6ImpvaG5zbWl0aCIsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpbImFtb3J0aXphdGlvbi9zY2hlZHVsZSIsImxvYW5zL2RvY3VtZW50cy91cGxvYWQiXSwiSXNWYWxpZCI6ZmFsc2V9.F_0aDeBgLpNGBiD5IVVAgEN07swGpnbhg8-yTWFzqX0';
        var api = apiStub(token);

        switch (configuration.activity) {
          case 'restful-invoke':
              var endpointGenerator = new Function('$set', '$get', '$transform',
                                           '$setStatus','$getStatus',
                                           '$configuration','$join', 'value',
                                           `return ${configuration.endpoint}`);

              var payloadGenerator = new Function('$set', '$get', '$transform',
                                                  '$setStatus','$getStatus',
                                                  '$configuration', '$join',
                                                  '$', 'value',
                                                  'return $(' + configuration.payload + ')');

               self.$execute = (requestId, value) => {

                 return new Promise((resolve, reject) =>
                      Promise.all(
                      [endpointGenerator(
                       self.$set.bind(self, requestId),
                       self.$get.bind(self, requestId),
                       self.$transform,
                       self.$setStatus.bind(self, requestId),
                       self.$getStatus.bind(self, requestId),
                       self.$configuration(api).get,
                       self.$join,
                       value
                     ),
                     payloadGenerator(
                      self.$set.bind(self, requestId),
                      self.$get.bind(self, requestId),
                      self.$transform,
                      self.$setStatus.bind(self, requestId),
                      self.$getStatus.bind(self, requestId),
                      self.$configuration(api).get,
                      self.$join,
                      interpolate,
                      value
                    )
                  ]).then((values) => {
                          debug('endpoint and payload', values);
                          var endpoint = values[0];
                          var payload = values[1];
                          api[configuration.type](endpoint, payload)
                          .then((value) => {
                              debug('response', value.body, configuration.type, endpoint, payload)
                              resolve(value.body);
                              return value;
                          })
                          .catch((err) => {
                            debug('request failed for endpoing', configuration.type, value, payload)
                            reject(err);
                          });
                      })
                     .catch((err) => {
                       debug('failed to load endpoint configuration', configuration, err);
                     })
                );

               };
        break;
          case 'lambda':
              self.$execute = (requestId, value) => {
                return new Promise((resolve, reject) => {
                  try{
                    resolve(configuration.body({
                      '$set': self.$set.bind(self, requestId), 
                      '$get': self.$get.bind(self, requestId),
                      '$transform': self.$transform,
                      '$setStatus': self.$setStatus.bind(self, requestId),
                      '$getStatus': self.$getStatus.bind(self, requestId),
                      '$configuration': self.$configuration(api).get,
                      '$api': api,
                      '$facts': _.reduce(action.fact.stage.facts, (p, c, prop) => {
                          p[prop] = {
                            '$get': c.$get.bind(self, requestId)
                          };
                          return p;
                        }, {}), 
                      '$join': self.$join,
                      '$': interpolate,
                      '$debug': Debug(`engine:workflow:/${action.fact.stage.name}/${action.fact.name}/${action.name}/${name}`), 
                      'value': value
                    }));
                  } catch(e){
                    reject(e);
                  }
                });
              }
          break;
          default:
            debug('Unable to load activity', configuration.name);
        }
    }
  }



  return self;
};
