module.exports = {
    "lead": {
        "bapplication": {
            "test2": ({ value, $facts }) => {
               return $facts['application'].$get('some-value'); 
            } 
        },
        "application": {
            "test": ({ value, $set }) => {
               return $set('some-value', value); 
            }, 
            "test2": ({ value, $get }) => {
               return $get('some-value'); 
            } 
        }
    }
};
