const debug = require('../debug')('engine:fact');
const createAction = require('./action');
const _ = require('lodash');

module.exports = function (stage, name, configuration) {

  var self = { };

  //Register this fact with stage.
  stage.facts[name] = self;

  self.stage = stage;
  self.name = name;
  self.$set =  stage.$set.bind(stage, name);
  self.$get = stage.$get.bind(stage, name);
  self.$transform = stage.$transform.bind(stage, name);
  self.$setStatus = stage.$setStatus.bind(stage, name);
  self.$getStatus = stage.$getStatus.bind(stage, name);
  self.actions = { };
  self.createAction = createAction.bind(createAction, self);


  _.forEach(configuration, (action, name) => {
    debug('loading action', name)
    self.createAction(name, action);
  });

  return self;
};
