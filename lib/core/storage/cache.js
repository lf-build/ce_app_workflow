var Promise = require('bluebird');
var _ = require('lodash');
var debug = require('../debug')('storage:cache');
var dummyStore = {

}

module.exports = {
   get (...keys) {
    let leafKey = _.last(keys);

    return new Promise(function (resolve, reject) {
      var result = _.reduce(keys, (a, c, index) => {
        if(!a){
          return a;
        }

        if(index === keys.length - 1 && c === leafKey){
          return a[c];
        }else {
          if(typeof a[c] !== 'object'){
            return undefined;
          }
        }

        return a[c] ? a[c] : a[c] = { };
      }, dummyStore);

      if(!(_.isUndefined(result) || _.isNull(result))){
        resolve(result);
      }else {
        reject({ 'not found': keys });
      }
    });
  },

  set(...args) {
    return new Promise(function (resolve, reject) {
      let [keys, [value]] = _.chunk(args, args.length -1 );
      let leafKey = _.last(keys);

      _.reduce(keys, (a, c, index) => {
          return a[c] && typeof a[c] === 'object'
                      ? a[c] = (index === keys.length -1 && c === leafKey ? value : a[c])
                      : a[c] = (
                            index === keys.length -1 && c === leafKey
                            ? value
                            : { }
                        );
      }, dummyStore);

      resolve(value);
    });
  }
}
