var Promise = require('bluebird');
var _ = require('lodash');
var debug = require('../debug')('storage');
var cache = require('./cache');
var db = require('./cache');//require('./persistent');

module.exports = {
get(...keys) {
    return new Promise(function (resolve, reject) {
      cache.get.apply(cache, keys)
           .then(value => {
             debug('got', keys, value);
             resolve(value);
           })
           .catch(err => {
             debug('failed to get', keys, err)
             db.get.apply(db, keys)
               .then(value => {
                 resolve(value);
               })
               .catch(err => {
                 reject(err);
               });
           });
    });
  },
  set(...args) {
     debug('setting', args);
     return Promise.all([cache.set.apply(cache, args), db.set.apply(cache, args)])
                   .then((value) => {
                     debug('value set', args, value.length ? value[0] : undefined)
                     return  value.length ? value[0] : undefined;
                   })
                   .catch((err) => {
                     debug('failed to set', args, err);
                   });
  }
}











// {
//     set: function(storeName, context, key, value){
//         return new Promise(function(resolve, reject){
//             if(!dummyStore[storeName]){
//                 dummyStore[storeName] = { };
//             }
//             if(!dummyStore[storeName][context]){
//                 dummyStore[storeName][context] = { };
//             }
//
//             dummyStore[storeName][context] = value;
//             resolve(value);
//         });
//     },
//     get: function(storeName, context, key){
//         return new Promise(function(resolve, reject){
//             if(dummyStore[storeName] && dummyStore[storeName][context]){
//                 resolve(dummyStore[key]);
//             } else{
//                 reject(404);
//             }
//         })
//     }
// }
