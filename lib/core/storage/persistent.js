var Promise = require('bluebird');
var _ = require('lodash');
var debug = require('../debug')('storage:persistent');
var dummyStore = {

}

module.exports = {
   get (...keys) {
    let leafKey = _.last(keys);

    return new Promise(function (resolve, reject) {
      var result = _.reduce(keys, (a, c) => {
        if(c === leafKey){
          return a[c];
        }
        return a[c] ? a[c] : a[c] = { };
      }, dummyStore);

      if(result){
        resolve(result);
      } else {
        reject('not found');
      }
    });
  },

  set(...args) {
    return new Promise(function (resolve, reject) {
      let [keys, [value]] = _.chunk(args, args.length -1 );
      let leafKey = _.last(keys);

      _.reduce(keys, (a, c) => {
        return a[c] && typeof a[c] === 'object' ? a[c] : a[c] = ( c === leafKey ? value : { } );
      }, dummyStore);

      resolve(value);
    });
  }
}











// {
//     set: function(storeName, context, key, value){
//         return new Promise(function(resolve, reject){
//             if(!dummyStore[storeName]){
//                 dummyStore[storeName] = { };
//             }
//             if(!dummyStore[storeName][context]){
//                 dummyStore[storeName][context] = { };
//             }
//
//             dummyStore[storeName][context] = value;
//             resolve(value);
//         });
//     },
//     get: function(storeName, context, key){
//         return new Promise(function(resolve, reject){
//             if(dummyStore[storeName] && dummyStore[storeName][context]){
//                 resolve(dummyStore[key]);
//             } else{
//                 reject(404);
//             }
//         })
//     }
// }
