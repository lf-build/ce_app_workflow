const _ = require('lodash');
const debug = require('./debug')('interpolate');

module.exports = (obj) => {
  return interpolate(obj);
};

function interpolate(obj) {
  if(!obj){
    return new Promise((resolve) => resolve(obj));
  }
  if(_.isFunction(obj.then)){
    return obj;
  }

  var waitForMe = _.reduce(obj, (a, c, prop) => {
      if( _.isFunction(c.then)){
        c.then( value => obj[prop] = value )
         .catch(err => { debug('failed',err) });
        a.push(c);
      } else if(_.isObject(c)) {
        a.push(interpolate(c));
      }
      return a;
    }, []);

    return Promise.all(waitForMe).then(value => obj);
}
