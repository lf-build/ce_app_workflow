FROM node:slim

WORKDIR /app
ADD /package.json /app/package.json
RUN npm install

RUN npm install -g pm2

ADD . /app

ENTRYPOINT pm2 start --no-daemon ./bin/www -i 1
