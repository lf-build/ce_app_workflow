var should = require('should');
var assert = require('assert');

var engine = require('../lib/core/engine');
var config = require('./engine-config');

describe('Engine', function () {
  var root = engine(config);

  describe('Stage', function () {

    var dummyStage = root.stages['lead'];

    it('must set proper name', function () {
      dummyStage.name.should.equal('lead');
    });
    describe('$set', function () {
      it('must be defined', function () {
        assert.notEqual(dummyStage.$set, undefined);
      });
      it('must be able to set value under single level key', function () {
        dummyStage.$set('mobile', '9898344754');
      });
      it('must be able to set value under multilevel key', function () {
        dummyStage.$set('country', 'code', '+91');
      });
    });

    describe('$get', function () {
      it('must be defined', function () {
        assert.notEqual(dummyStage.$get, undefined);
      });
      it('must be able to get value under single level key', function () {
        dummyStage.$get('mobile')
                  .then(value => {
                    value.should.equal('9898344754');
                  })
                  .catch(err => {
                    assert.fail('', err);
                  });
      });
      it('must be able to get value under multilevel key', function () {
        dummyStage.$get('country', 'code')
                .then(value => {
                  value.should.equal('+91');
                })
                .catch(err => {
                  assert.fail('', err);
                });

      });
    });

    describe('$setStatus', function () {
      it('must defined', function () {
        assert.notEqual(dummyStage.$setStatus, undefined);
      });
    });
    describe('$getStatus', function () {
      it('must defined', function () {
        assert.notEqual(dummyStage.$getStatus, undefined);
      });
    });
    describe('$transform', function () {
      it('must defined', function () {
        assert.notEqual(dummyStage.$transform, undefined);
      });
    });


    describe('Fact', function () {
      var dummyFact = dummyStage.facts['mobile'];

      it('must define $set macro', function () {
        assert.notEqual(dummyFact.$set, undefined);
      });
      it('must define $get macro', function () {
        assert.notEqual(dummyFact.$get, undefined);
      });
      it('must define $setStatus macro', function () {
        assert.notEqual(dummyFact.$setStatus, undefined);
      });
      it('must define $getStatus macro', function () {
        assert.notEqual(dummyFact.$getStatus, undefined);
      });
      it('must define $transform macro', function () {
        assert.notEqual(dummyFact.$transform, undefined);
      });


      describe('actions', function () {
        var dummyAction = dummyFact.actions['set-number'];

        it('must define $set macro', function () {
          assert.notEqual(dummyAction.$set, undefined);
        });
        it('must define $get macro', function () {
          assert.notEqual(dummyAction.$get, undefined);
        });
        it('must define $setStatus macro', function () {
          assert.notEqual(dummyAction.$setStatus, undefined);
        });
        it('must define $getStatus macro', function () {
          assert.notEqual(dummyAction.$getStatus, undefined);
        });
        it('must define $transform macro', function () {
          assert.notEqual(dummyAction.$transform, undefined);
        });

        it('must execute without throwing exception', function (done) {
          dummyAction.$execute('987', { 'mobile' : '9898989898' })
                     .then(value => {
                       try {
                         value.should.deepEqual({
                           mobile: '9898989898',
                           hint: 'set-number/return'
                         });
                          //done();
                       } catch (e) {
                         //done(e);
                       }
                      finally{

                          dummyFact.actions['send-otp'].$execute('987')
                          .then((value) => {
                              try {
                              value.should.deepEqual({
                                "ref": "otp-test-ref-number-123",
                                "hint": 'verify-otp'
                              });
                              //done();
                              } catch (e) {
                              //  done(e);
                              } finally {


                                dummyFact.actions['verify-otp'].$execute('987', {otp: '123'})
                                .then((value) => {
                                    try {
                                    value.should.deepEqual({
                                        "verified": true
                                    });
                                    done();
                                    } catch (e) {
                                      done(e);
                                    } finally {

                                    }
                                })
                                .catch((err) => {
                                  console.log('failed', err);
                                });




                              }
                          })
                          .catch((err) => {
                            console.log('failed', err);
                          })
                      }


                      });




        });

        describe('activity', function () {
          var dummyActivity = dummyAction.activities['return'];

          it('must define $set macro', function () {
            assert.notEqual(dummyActivity.$set, undefined);
          });
          it('must define $get macro', function () {
            assert.notEqual(dummyActivity.$get, undefined);
          });
          it('must define $setStatus macro', function () {
            assert.notEqual(dummyActivity.$setStatus, undefined);
          });
          it('must define $getStatus macro', function () {
            assert.notEqual(dummyActivity.$getStatus, undefined);
          });
          it('must define $transform macro', function () {
            assert.notEqual(dummyActivity.$transform, undefined);
          });

          describe('$execute', function () {
            it('must execute without exception', function () {
              dummyAction.activities['save-to-store'].$execute('123', { 'mobile': '9898344754' });
              dummyActivity.$execute('123', { 'mobile': '9898344754' });
            });

          });

        });
      });


    });

  });




});
