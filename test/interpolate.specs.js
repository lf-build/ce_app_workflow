const interpolate = require('../lib/core/interpolate');
const should = require('should');

describe('Interpolator', function () {
  it('interpolates single level objects', function (done) {

    var target = {
      id: new Promise((resolve, reject) => resolve(999)),
      name: new Promise((resolve, reject) => resolve('Amit Dave')),
      address: 'Address'
    }

    interpolate(target).then(value => {

          try {
                value.should.deepEqual({
                  id: 999,
                  name: 'Amit Dave',
                  address: 'Address'
                });
                done();
          } catch (e) {
            done(e)
          } ;
    })
    .catch(err => {
      done(err);
    });
  });
  it('interpolates multilevel objects', function (done) {
        var target = {
          id: new Promise((resolve, reject) => resolve(999)),
          name: new Promise((resolve, reject) => resolve('Amit Dave')),
          address: {
            city: new Promise((resolve, reject) => resolve('Ahmedabad')),
            soc: {
              name: new Promise((resolve, reject) => resolve('Home'))
            }
          }
        }

        interpolate(target)
          .then(value => {
             try {
                   value
                   .should
                       .deepEqual({
                         id: 999,
                         name: 'Amit Dave',
                         address: {
                             city: 'Ahmedabad',
                             soc: {
                               name: 'Home'
                             }
                           }
                       });
                       done();
                 } catch (e) {
                   done(e);
                 };

            });
  });

});
