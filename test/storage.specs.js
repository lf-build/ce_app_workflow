var should = require('should');
var assert = require('assert');

var storage = require('../lib/core/storage');

describe('Storage', function () {
  describe('it should support single level keys while', function () {
    it('getting & setting', function () {
      storage.set('cool', 9)
             .then((data) => {
               storage.get('cool')
                      .then((data) => {
                        data.should.equal(9);
                      })
                      .catch(err => {
                        console.log('Stupid error');
                      });
             });
    });
  });

  describe('it should support multilevel keys while', function () {
    it('getting & setting', function () {
      storage.set('cool', 'stuff', 9)
             .then((data) => {
               storage.get('cool', 'stuff')
                      .then((data) => {
                        data.should.equal(9);
                      })
                      .catch(err => {
                        console.log('Stupid error');
                      });
             });
    });
  });
});
