var express = require('express');
var router = express.Router();
var debug = require('debug')('ce_app_workflow:index');

 

router.post('/:key', function(req, res, next) {
  req.store
     .set(req.params.key, req.body)
     .then(function(data){
        res.status(201).json(data);
     });
});

router.get('/:key', function(req, res, next) {
  req.store
     .get(req.params.key)
     .then(function(data){
        res.status(200).json(data);
     });
});

module.exports = router;
